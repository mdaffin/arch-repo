FROM rust:alpine AS builder

RUN apk add --no-cache musl-dev openssl-dev

WORKDIR /app
ENV RUSTFLAGS="-C target-feature=-crt-static" 
RUN cargo new --lib lib
RUN cargo new controller
COPY Cargo.lock controller/Cargo.toml controller/
COPY lib/Cargo.toml lib/

WORKDIR /app/controller
RUN cargo build --release

COPY lib/src /app/lib/src
RUN touch /app/lib/src/lib.rs
COPY controller/src src
RUN touch src/main.rs
RUN cargo build --release

FROM gcr.io/distroless/static
COPY --from=builder \
    /lib/ld-musl-x86_64.so.1 \
    /lib/libssl.so.1.1 \
    /lib/libcrypto.so.1.1 \
    /usr/lib/libgcc_s.so.1 \
    /lib/ld-musl-x86_64.so.1 \
    /lib/

COPY --from=builder /app/controller/target/release/aurora-controller /bin/aurora-controller

USER nonroot
ENTRYPOINT []
CMD ["/bin/aurora-controller"]
