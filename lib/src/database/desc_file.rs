use std::convert::TryFrom;
use std::io::Read;
use std::{convert::Infallible, str::FromStr};
use tar::Archive;

use crate::package::PkgInfo;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct DbDesc {
    pub name: Option<String>,
    pub version: Option<String>,
    pub filename: Option<String>,
    pub md5sum: Option<String>,
    pub sha256sum: Option<String>,
    pub base: Option<String>,
    pub desc: Option<String>,
    pub url: Option<String>,
    pub arch: Option<String>,
    pub builddate: Option<String>,
    pub packager: Option<String>,
    pub isize: Option<i64>,
    pub csize: Option<i64>,
    pub groups: Vec<String>,
    pub licenses: Vec<String>,
    pub conflicts: Vec<String>,
    pub replaces: Vec<String>,
    pub provides: Vec<String>,
    pub depends: Vec<String>,
    pub optdepends: Vec<String>,
    pub makedepends: Vec<String>,
    pub checkdepends: Vec<String>,
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct FilesDesc {
    pub files: Vec<String>,
}

impl ToString for DbDesc {
    fn to_string(&self) -> String {
        let mut desc = String::new();
        if let Some(name) = &self.name {
            desc += &format!("%NAME%\n{}\n\n", name);
        }
        if let Some(version) = &self.version {
            desc += &format!("%VERSION%\n{}\n\n", version);
        }
        if let Some(filename) = &self.filename {
            desc += &format!("%FILENAME%\n{}\n\n", filename);
        }
        if let Some(md5sum) = &self.md5sum {
            desc += &format!("%MD5SUM%\n{}\n\n", md5sum);
        }
        if let Some(sha256sum) = &self.sha256sum {
            desc += &format!("%SHA256SUM%\n{}\n\n", sha256sum);
        }
        if let Some(base) = &self.base {
            desc += &format!("%BASE%\n{}\n\n", base);
        }
        if let Some(description) = &self.desc {
            if !description.is_empty() {
                desc += &format!("%DESC%\n{}\n\n", description);
            }
        }
        if let Some(url) = &self.url {
            if !url.is_empty() {
                desc += &format!("%URL%\n{}\n\n", url);
            }
        }
        if let Some(arch) = &self.arch {
            desc += &format!("%ARCH%\n{}\n\n", arch);
        }
        if let Some(builddate) = &self.builddate {
            desc += &format!("%BUILDDATE%\n{}\n\n", builddate);
        }
        if let Some(packager) = &self.packager {
            desc += &format!("%PACKAGER%\n{}\n\n", packager);
        }
        if let Some(isize) = &self.isize {
            desc += &format!("%ISIZE%\n{}\n\n", isize);
        }
        if let Some(csize) = &self.csize {
            desc += &format!("%CSIZE%\n{}\n\n", csize);
        }
        if !self.groups.is_empty() {
            desc += &format!("%GROUPS%\n{}\n\n", self.groups.join("\n"));
        }
        if !self.licenses.is_empty() {
            desc += &format!("%LICENSE%\n{}\n\n", self.licenses.join("\n"));
        }
        if !self.conflicts.is_empty() {
            desc += &format!("%CONFLICTS%\n{}\n\n", self.conflicts.join("\n"));
        }
        if !self.replaces.is_empty() {
            desc += &format!("%REPLACES%\n{}\n\n", self.replaces.join("\n"));
        }
        if !self.provides.is_empty() {
            desc += &format!("%PROVIDES%\n{}\n\n", self.provides.join("\n"));
        }
        if !self.depends.is_empty() {
            desc += &format!("%DEPENDS%\n{}\n\n", self.depends.join("\n"));
        }
        if !self.optdepends.is_empty() {
            desc += &format!("%OPTDEPENDS%\n{}\n\n", self.optdepends.join("\n"));
        }
        if !self.makedepends.is_empty() {
            desc += &format!("%MAKEDEPENDS%\n{}\n\n", self.makedepends.join("\n"));
        }
        if !self.checkdepends.is_empty() {
            desc += &format!("%CHECKDEPENDS%\n{}\n\n", self.checkdepends.join("\n"));
        }
        desc
    }
}

impl From<PkgInfo> for DbDesc {
    fn from(src: PkgInfo) -> DbDesc {
        DbDesc {
            name: Some(src.pkgname),
            base: src.pkgbase,
            version: Some(src.pkgver),
            desc: src.pkgdesc,
            isize: src.size,
            url: src.url,
            arch: src.arch,
            builddate: src.builddate,
            packager: src.packager,
            groups: src.groups,
            licenses: src.licenses,
            replaces: src.replaces,
            conflicts: src.conflicts,
            provides: src.provides,
            depends: src.depends,
            optdepends: src.optdepends,
            makedepends: src.makedepends,
            checkdepends: src.checkdepends,
            ..Default::default()
        }
    }
}

impl FromStr for DbDesc {
    type Err = Infallible;
    fn from_str(contents: &str) -> Result<Self, Self::Err> {
        fn get_value<'a>(mut iter: impl Iterator<Item = &'a str>) -> Option<String> {
            let line = iter.next().map(str::to_string);
            if line.as_ref().map(String::is_empty).unwrap_or(true) {
                None
            } else {
                line
            }
        }

        fn get_list<'a>(mut iter: impl Iterator<Item = &'a str>, list: &mut Vec<String>) {
            loop {
                match iter.next() {
                    None => return,
                    Some("") => return,
                    Some(package) => list.push(package.to_string()),
                }
            }
        }

        let mut iter = contents.lines();
        let mut desc = DbDesc::default();

        loop {
            match iter.next() {
                None => break,
                Some("%NAME%") => desc.name = get_value(&mut iter),
                Some("%VERSION%") => desc.version = get_value(&mut iter),
                Some("%FILENAME%") => desc.filename = get_value(&mut iter),
                Some("%MD5SUM%") => desc.md5sum = get_value(&mut iter),
                Some("%SHA256SUM%") => desc.sha256sum = get_value(&mut iter),
                Some("%BASE%") => desc.base = get_value(&mut iter),
                Some("%DESC%") => desc.desc = get_value(&mut iter),
                Some("%URL%") => desc.url = get_value(&mut iter),
                Some("%ARCH%") => desc.arch = get_value(&mut iter),
                Some("%BUILDDATE%") => desc.builddate = get_value(&mut iter),
                Some("%PACKAGER%") => desc.packager = get_value(&mut iter),
                Some("%ISIZE%") => {
                    desc.isize = get_value(&mut iter).map(|v| v.parse::<i64>().unwrap_or(-1))
                }
                Some("%CSIZE%") => {
                    desc.csize = get_value(&mut iter).map(|v| v.parse::<i64>().unwrap_or(-1))
                }
                Some("%GROUPS%") => get_list(&mut iter, &mut desc.groups),
                Some("%LICENSE%") => get_list(&mut iter, &mut desc.licenses),
                Some("%CONFLICTS%") => get_list(&mut iter, &mut desc.conflicts),
                Some("%REPLACES%") => get_list(&mut iter, &mut desc.replaces),
                Some("%PROVIDES%") => get_list(&mut iter, &mut desc.provides),
                Some("%DEPENDS%") => get_list(&mut iter, &mut desc.depends),
                Some("%OPTDEPENDS%") => get_list(&mut iter, &mut desc.optdepends),
                Some("%MAKEDEPENDS%") => get_list(&mut iter, &mut desc.makedepends),
                Some("%CHECKDEPENDS%") => get_list(&mut iter, &mut desc.checkdepends),
                _ => {}
            };
        }

        Ok(desc)
    }
}

impl ToString for FilesDesc {
    fn to_string(&self) -> String {
        format!("%Files%\n{}\n", self.files.join("\n"))
    }
}

impl<R: Read> TryFrom<Archive<R>> for FilesDesc {
    type Error = std::io::Error;

    fn try_from(mut src: Archive<R>) -> Result<Self, Self::Error> {
        Ok(FilesDesc {
            files: src
                .entries()?
                .map(|entry| match entry {
                    Ok(entry) => Ok(entry.path()?.to_str().unwrap().to_string()),
                    Err(entry) => Err(entry),
                })
                .filter(|entry| {
                    let p = entry.as_deref().unwrap_or_default();
                    p != ".BUILDINFO" && p != ".MTREE" && p != ".PKGINFO"
                })
                .collect::<Result<Vec<_>, _>>()?,
        })
    }
}

impl FromStr for FilesDesc {
    type Err = Infallible;
    fn from_str(contents: &str) -> Result<Self, Self::Err> {
        fn get_list<'a>(mut iter: impl Iterator<Item = &'a str>, list: &mut Vec<String>) {
            loop {
                match iter.next() {
                    None => return,
                    Some("") => return,
                    Some(package) => list.push(package.to_string()),
                }
            }
        }

        let mut iter = contents.lines();
        let mut desc = FilesDesc::default();

        loop {
            match iter.next() {
                None => break,
                Some("%FILES%") => get_list(&mut iter, &mut desc.files),
                _ => {}
            };
        }

        Ok(desc)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use test_case::test_case;

    #[test]
    fn parsing_and_empty_contents_is_valid() {
        let desc = DbDesc::from_str("");
        assert_eq!(Ok(DbDesc::default()), desc);
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(50))]
        #[test]
        fn to_string_with_name_produces_that_name_in_the_output(desc in arb_desc()) {
            let output = desc.to_string();
            if let Some(name) = desc.name {
                assert!(output.contains(&name));
            }
        }
    }

    #[test_case("%NAME%")]
    #[test_case("%NAME%\n")]
    #[test_case("%NAME%\n\n%FILENAME%\n")]
    #[test_case("%NAME%\n\n\n\n%FILENAME%\n\n\n")]
    fn value_missing_from_key_default_to_none(name: &str) {
        let desc = DbDesc::from_str(name);
        assert_eq!(Ok(DbDesc::default()), desc);
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(50))]
        #[test]
        fn to_string_then_from_str_produces_the_same_desc(desc in arb_desc()) {
            let new_desc = DbDesc::from_str(&desc.to_string());
            assert_eq!(Ok(desc), new_desc);
        }
    }

    fn arb_desc() -> impl Strategy<Value = DbDesc> {
        use proptest::collection::vec;
        use proptest::option::weighted;
        (
            (
                weighted(0.1, "\\PC+"),
                weighted(0.1, "\\PC+"),
                weighted(0.1, "\\PC+"),
                weighted(0.1, "\\PC+"),
                weighted(0.1, "\\PC+"),
                weighted(0.4, "\\PC+"),
                weighted(0.4, "\\PC+"),
                weighted(0.4, "\\PC+"),
                weighted(0.4, "\\PC+"),
                weighted(0.4, "\\PC+"),
                weighted(0.4, "\\PC+"),
            ),
            weighted(0.1, any::<i64>()),
            weighted(0.1, any::<i64>()),
            (
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
                vec("\\PC+", 0..10),
            ),
        )
            .prop_map(
                |(
                    (
                        name,
                        version,
                        filename,
                        md5sum,
                        sha256sum,
                        base,
                        desc,
                        url,
                        arch,
                        builddate,
                        packager,
                    ),
                    isize,
                    csize,
                    (
                        groups,
                        licenses,
                        conflicts,
                        replaces,
                        provides,
                        depends,
                        optdepends,
                        makedepends,
                        checkdepends,
                    ),
                )| DbDesc {
                    name,
                    version,
                    filename,
                    md5sum,
                    sha256sum,
                    base,
                    desc,
                    url,
                    arch,
                    builddate,
                    packager,
                    isize,
                    csize,
                    groups,
                    licenses,
                    conflicts,
                    replaces,
                    provides,
                    depends,
                    optdepends,
                    makedepends,
                    checkdepends,
                },
            )
    }
}
