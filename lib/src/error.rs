use http::Uri;
use std::{io, path::PathBuf};
use thiserror::Error;

pub type Result<T> = std::result::Result<T, AuroraError>;

#[derive(Error, Debug)]
pub enum AuroraError {
    #[error("missing bucket from s3 uri: {0}")]
    MissingBucketFromS3Uri(Uri),
    #[error("missing filename from s3 uri: {0}")]
    MissingFileFromS3Uri(PathBuf),
    #[error("uri does not have the s3:// scheme: {0}")]
    InvalidS3Scheme(Uri),
    #[error("failed to backup database '{db_file}' to '{backup_file}'")]
    DbBackupFailure {
        db_file: PathBuf,
        backup_file: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("failed to write database '{db_file}'")]
    DbWriteFailure {
        db_file: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("invalid s3 uri: '{s3_uri}'")]
    InvalidS3Uri {
        s3_uri: String,
        #[source]
        src: http::uri::InvalidUri,
    },
    #[error("invalid AWS_ENDPOINT_URL environment variable: '{endpoint_url}'")]
    InvalidAwsEndpointUrl {
        endpoint_url: String,
        #[source]
        src: http::uri::InvalidUri,
    },
    #[error("{0}")]
    IoError(#[from] io::Error),
    #[error("{0}")]
    S3Error(#[from] aws_sdk_s3::Error),
    #[error("{0}")]
    AwsSdkGetError(#[from] aws_sdk_s3::SdkError<aws_sdk_s3::error::GetObjectError>),
    #[error("{0}")]
    AwsSdkPutError(#[from] aws_sdk_s3::SdkError<aws_sdk_s3::error::PutObjectError>),
    #[error("{0}")]
    AwsSdkDeleteError(#[from] aws_sdk_s3::SdkError<aws_sdk_s3::error::DeleteObjectError>),
    #[error("{0}")]
    ReadBodyError(#[from] aws_smithy_http::byte_stream::Error),
}

impl AuroraError {
    pub fn is_not_exists(&self) -> bool {
        use AuroraError::*;
        match self {
            AwsSdkGetError(aws_sdk_s3::SdkError::ServiceError {
                err:
                    aws_sdk_s3::error::GetObjectError {
                        kind: aws_sdk_s3::error::GetObjectErrorKind::NoSuchKey(_),
                        ..
                    },
                ..
            }) => true,
            _ => false,
        }
    }
}
