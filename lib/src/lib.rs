use std::{
    ffi::OsStr,
    fs,
    os::unix::prelude::OsStrExt,
    path::{Path, PathBuf},
    str::FromStr,
};

use aws_config::meta::region::ProvideRegion;
use bytes::Buf;
use http::Uri;
use log::*;

pub mod database;
pub mod error;
pub mod package;
#[cfg(test)]
mod tests;

use error::{AuroraError, Result};

pub use database::Database;
pub use package::Package;

#[derive(Debug)]
pub struct DbPath(Uri);

impl DbPath {
    pub async fn fetch_db(&self, client: aws_sdk_s3::Client) -> Result<Database> {
        let uri = &self.0;
        Ok(Self::open_s3_database(&client, Self::bucket_from(uri)?, uri.path()).await?)
    }

    pub async fn fetch_files(&self, client: aws_sdk_s3::Client) -> Result<Database> {
        let uri = &self.0;
        Ok(Self::open_s3_database(
            &client,
            Self::bucket_from(uri)?,
            Self::db_to_files_path(uri.path()).to_str().unwrap(),
        )
        .await?)
    }

    pub async fn write_db(&self, client: aws_sdk_s3::Client, database: &Database) -> Result<()> {
        let uri = &self.0;
        let mut db_file = Self::create_tmp_database_file_for(uri.path())?;
        Self::write_database(database, &mut db_file)?;
        debug!("Uploading database to '{}'", uri);
        Self::upload_s3(&client, Self::bucket_from(uri)?, uri.path(), db_file.path()).await?;
        Ok(())
    }

    pub async fn write_files(&self, client: aws_sdk_s3::Client, database: &Database) -> Result<()> {
        let uri = &self.0;
        let bucket = Self::bucket_from(uri)?;
        let path = Self::db_to_files_path(uri.path());
        let path = path.to_str().unwrap();
        let mut db_file = Self::create_tmp_database_file_for(path)?;
        Self::write_database(database, &mut db_file)?;
        debug!("Uploading database to 's3://{}{}'", bucket, path);
        Self::upload_s3(&client, bucket, path, db_file.path()).await?;
        Ok(())
    }

    pub async fn write_package(
        &self,
        client: aws_sdk_s3::Client,
        package: impl AsRef<Path>,
    ) -> Result<()> {
        let uri = &self.0;
        let package = package.as_ref();
        let package_name = package.file_name().unwrap();
        let bucket = Self::bucket_from(uri)?;
        let s3_prefix = Self::db_to_files_path(uri.path());
        let s3_prefix = s3_prefix.parent().unwrap();
        let path = s3_prefix.join(package_name);
        debug!("Uploading package to 's3://{}{}'", bucket, path.display());
        Self::upload_s3(&client, bucket, path.to_str().unwrap(), package).await?;
        Ok(())
    }

    pub async fn remove_package(
        &self,
        client: aws_sdk_s3::Client,
        package: impl AsRef<str>,
    ) -> Result<()> {
        let uri = &self.0;
        let bucket = Self::bucket_from(uri)?;
        let s3_prefix = Self::db_to_files_path(uri.path());
        let s3_prefix = s3_prefix.parent().unwrap();
        let key = s3_prefix
            .join(package.as_ref())
            .to_str()
            .unwrap()
            .to_string();
        debug!("Removing package from 's3://{}{}'", bucket, key);
        client
            .delete_object()
            .bucket(bucket)
            .key(key)
            .send()
            .await?;
        Ok(())
    }

    async fn open_s3_database(
        client: &aws_sdk_s3::Client,
        bucket: impl Into<String>,
        path: impl Into<String>,
    ) -> Result<Database> {
        match Self::fetch_s3(client, bucket, path).await {
            Ok(db_file) => Ok(Database::from_file(db_file.path())?),
            Err(err) if err.is_not_exists() => Ok(Database::new()?),
            Err(err) => Err(err),
        }
    }

    fn bucket_from(uri: &Uri) -> Result<&str> {
        uri.host()
            .ok_or_else(|| AuroraError::MissingBucketFromS3Uri(uri.clone()))
    }

    async fn fetch_s3(
        client: &aws_sdk_s3::Client,
        bucket: impl Into<String>,
        key: impl Into<String>,
    ) -> Result<tempfile::NamedTempFile> {
        let key = key.into();
        let mut dest = Self::create_tmp_database_file_for(&key)?;

        let resp = client.get_object().bucket(bucket).key(key).send().await?;
        let data = resp.body.collect().await?;

        std::io::copy(&mut data.reader(), &mut dest)?;

        Ok(dest)
    }

    async fn upload_s3(
        client: &aws_sdk_s3::Client,
        bucket: impl Into<String>,
        key: impl Into<String>,
        db_file: impl AsRef<Path>,
    ) -> Result<()> {
        client
            .put_object()
            .bucket(bucket)
            .key(key.into())
            .body(aws_sdk_s3::ByteStream::from_path(db_file).await?)
            .send()
            .await?;

        Ok(())
    }

    fn create_tmp_database_file_for(db_path: impl AsRef<Path>) -> Result<tempfile::NamedTempFile> {
        let db_path = db_path.as_ref();
        let file_name = db_path
            .file_name()
            .ok_or_else(|| AuroraError::MissingFileFromS3Uri(db_path.to_path_buf()))
            .map(|f| f.to_str().unwrap())?;

        Ok(tempfile::Builder::new().suffix(file_name).tempfile()?)
    }

    fn db_to_files_path(db_file: impl AsRef<Path>) -> PathBuf {
        let db_file = db_file.as_ref();
        let (db_name, db_ext) = Self::split_db_file_name(db_file);
        let mut files_file = db_name.to_os_string();
        files_file.push(OsStr::new(".files."));
        files_file.push(db_ext);
        db_file.with_file_name(files_file)
    }

    fn split_db_file_name<'a>(db_file: &'a Path) -> (&'a OsStr, &'a OsStr) {
        let file_name = db_file.file_name().unwrap().as_bytes();
        let (index, _) = file_name
            .windows(4)
            .enumerate()
            .rev()
            .find(|(_, s)| s == b".db.")
            .unwrap();
        (
            OsStr::from_bytes(&file_name[..index]),
            OsStr::from_bytes(&file_name[index + 4..]),
        )
    }

    fn write_database(database: &Database, db_file: impl AsRef<Path>) -> Result<()> {
        let db_file = db_file.as_ref();
        let mut extension = db_file.extension().unwrap_or_default().to_os_string();
        extension.push(".old");
        let backup_file = db_file.with_extension(&extension);

        match fs::rename(db_file, &backup_file) {
            Ok(()) => {}
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => {}
            Err(err) => {
                return Err(AuroraError::DbBackupFailure {
                    db_file: db_file.to_path_buf(),
                    backup_file,
                    source: err,
                })
            }
        }

        database
            .write_to_file(db_file)
            .map_err(|err| AuroraError::DbWriteFailure {
                db_file: db_file.to_path_buf(),
                source: err,
            })?;
        Ok(())
    }
}

impl FromStr for DbPath {
    type Err = AuroraError;

    fn from_str(s: &str) -> Result<Self> {
        let uri = Uri::from_str(s).map_err(|err| AuroraError::InvalidS3Uri {
            s3_uri: s.to_string(),
            src: err,
        })?;
        if uri.scheme_str() == Some("s3") {
            Ok(Self(uri))
        } else {
            Err(AuroraError::InvalidS3Scheme(uri))
        }
    }
}

pub async fn create_s3_client(
    endpoint_url: Option<&http::Uri>,
    access_key_id: Option<&str>,
    secret_access_key: Option<&str>,
) -> std::result::Result<aws_sdk_s3::Client, http::uri::InvalidUri> {
    let region = aws_config::default_provider::region::default_provider()
        .region()
        .await
        .unwrap_or_else(|| aws_sdk_s3::Region::new("us-east-2"));

    let shared_config = aws_config::from_env().region(region).load().await;
    let config = aws_sdk_s3::config::Builder::from(&shared_config);

    let config = match endpoint_url {
        Some(endpoint_url) => {
            config.endpoint_resolver(aws_sdk_s3::Endpoint::immutable(endpoint_url.clone()))
        }
        None => match std::env::var("AWS_ENDPOINT_URL").ok().as_ref() {
            Some(endpoint_url) => {
                let endpoint_url = http::Uri::from_str(endpoint_url)?;
                if endpoint_url.scheme().is_none() {
                    todo!("invalid uri")
                }
                config.endpoint_resolver(aws_sdk_s3::Endpoint::immutable(endpoint_url))
            }
            None => config,
        },
    };

    let config = match (access_key_id, secret_access_key) {
        (Some(access_key_id), Some(secret_access_key)) => config.credentials_provider(
            aws_sdk_s3::Credentials::from_keys(access_key_id, secret_access_key, None),
        ),
        _ => config,
    };

    Ok(aws_sdk_s3::Client::from_conf(config.build()))
}
